Vamos a explicar como podemos tener Tryton en una nueva ventana para mejorar nuestra productividad.

Ventajas:
* No tendremos que movernos entre pestañas de nuestro navegador  
* Vamos a tener un icono en nuestra barra  
* No necesitamos todo el espacio que ocupa la URL y los distintos iconos que la acompañan  

# __Navegador GOOGLE CHROME__  
Primero de todo accedemos a nuestro Tryton, en este caso, hemos utilizado una demo de Tryton, pero en vuestro caso seria la url ofrecida por Kopen.  
![Alt text](1.jpg?raw=true "Demo Tryton")

Nos dirigimos a los tres puntos de la parte derecha del navegador, Luego a Más herramientas y finalmente a Crear acceso directo...  
![Alt text](2.jpg?raw=true "Menu Chrome > Mas herramientas > Crear acceso directo")

Se nos mostrara una pantalla como la siguiente, debemos importante que marquemos la opción de __"Abrir como ventana"__  
![Alt text](3.jpg?raw=true "Marcar Abrir como ventana")

A partir de ese momento veremos un nuevo icono en nuestro Escritorio  
![Alt text](4.jpg?raw=true "")

Si hacemos doble clic en el veremos como ya tenemos más espacio puesto que se ha eliminado la parte superior.  
![Alt text](5.jpg?raw=true "")

Finalmente solo nos queda hacer click derecho sobre el icono de nuestra barra y seleccionar "Anclar a la barra de tareas"  
![Alt text](6.jpg?raw=true "")

Solo falta comentar que las mismas opciones que podéis encontrar en Chrome como Copiar la URL, aumentar el tamaño de la página, etc.. se encontraran ahora en los tres puntos de la parte superior de la ventana.  
![Alt text](7.jpg?raw=true "")

__TIP:__ Copiar la URL nos permitirá enviarle a otro compañero de trabajo lo mismo que tenemos nosotros en pantalla
